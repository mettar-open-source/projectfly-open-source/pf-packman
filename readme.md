## projectFLY Packman Package

[![projectFLY 2.0](https://img.shields.io/badge/projectFLY-2.0-orange.svg?style=flat-square)](https://github.com/projectFLY/framework)
[![Source](http://img.shields.io/badge/source-projectFLY/packman-blue.svg?style=flat-square)](https://github.com/projectFLY/packman)

The projectFLY Packman package is responsible for providing the fundamental projectFLY package routing and namespacing functions between different packages, including packages that are currently being developed. In a nutshell, Packman provides an interface for all packages to use so as to bootstrap themselves with composer and the projectFLY framework itself .


## Documentation

Documentation for the packman package can be found on the [github wiki](https://github.com/projectFLY/packman/wiki).

