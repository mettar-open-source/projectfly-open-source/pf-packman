<?php

namespace Projectfly\Packman\Repository;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;

class Package
{

    /**
     * The directory of the package
     *
     * @var string
     */
    public $path;

    /**
     * The subdomain of the package
     *
     * @var string
     */
    public $subdomain;

    /**
     * The slug of the package
     *
     * @var string
     */
    public $slug;

    /**
     * The namespace of the package
     *
     * @var string
     */
    public $namespace;

    /**
     * Whether the package is under development.
     *
     * @var string
     */
    private $is_dev;

    /**
     * Instantiate new Package.
     *
     * @param  string $path
     * @return void
     */
    public function __construct($path, $is_dev = false)
    {
        $this->path = $path . '/';
        $this->validate_package();
        if ($is_dev) {
            $this->is_dev = true;
        }
    }

    /**
     * Validate a package.
     *
     * @param  string $path
     * @return void
     */
    public function validate_package()
    {
        $file = $this->base_path('packman.json');
        if (File::exists($file)) {
            $config = json_decode(File::get($file));
            $this->subdomain = $config->subdomain;
            $this->slug = $config->slug;
            $this->namespace = $config->namespace;
            return true;
        }

        return false;
    }

    /**
     * Bootstrap a Package.
     *
     * @return void
     */
    public function bootstrap()
    {
        if ($this->validate_package()) {
            if ($this->is_dev) {
                $loader = require base_path() . '/vendor/autoload.php';
                $loader->setPsr4($this->namespace . '\\', $this->source_path());
            }

            App::register($this->namespace . '\PackageServiceProvider');
        }
    }

   /**
     * Retrieve the base path of a Package.
     *
     * @param string $file
     * @return string
     */
    public function base_path($file = '')
    {
        return $this->path . $file;
    }

   /**
     * Retrieve the source path of a Package.
     *
     * @param string $file
     * @return string
     */
    public function source_path($file = '')
    {
        return $this->path . 'src/' . $file;
    }

   /**
     * Retrieve the source namespace of a Package.
     *
     * @param string $file
     * @return string
     */
    public function namespace($file = '')
    {
        return $this->namespace . '\\' . $file;
    }
}
