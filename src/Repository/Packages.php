<?php

namespace Projectfly\Packman\Repository;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Projectfly\Packman\Exceptions\PackageNotFound;

class Packages
{
    /**
     * All the packages
     *
     * @var array
     */
    protected $packages = [];

    /**
     * Instantiate new Packages.
     *
     * @param  array $packages
     * @return void
     */
    public function __construct($packages)
    {
        $this->packages = $packages;
    }

    /**
     * Get all packages
     *
     * @return array
     */
    public function all()
    {
        return $this->packages;
    }

    /**
     * Instantiate new Packages.
     *
     * @param  array $packages
     * @return package|false
     */
    public function findBySlug($slug)
    {
        foreach ($this->packages as $package) {
            if ($package->slug == $slug) {
                return $package;
            }
        }

        throw new PackageNotFound("No $slug active package found.");
    }


    /**
     * Check if a package exists.
     *
     * @param  string $slug
     * @return boolean
     */
    public function exists($slug)
    {
        foreach ($this->packages as $package) {
            if ($package->slug == $slug) {
                return true;
            }
        }
        
        return false;
    }
}
