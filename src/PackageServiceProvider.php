<?php

namespace Projectfly\Packman;

use Projectfly;
use ReflectionClass;
use Illuminate\Routing\Router;
use Projectfly\Packman\Repository\Package;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

abstract class PackageServiceProvider extends ServiceProvider
{

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace;

    /**
     * This package information that will be registered.
     *
     * @var object
     */
    protected $package;

    /**
     * The router to be used
     *
     * @var Router
     */
    protected $router;

    /**
     * The directory of the package
     *
     * @var Router
     */
    protected $directory;

    /**
     * Instantiate new PackageServiceProvider.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->directory = dirname((new ReflectionClass($this))->getFileName());
        $this->directory = str_replace('/src', '', $this->directory);
        $this->package = new Package($this->directory);
        $this->namespace = $this->package->namespace;
    }

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->router = $router;
        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $this->mapPackageRoutes([
            'namespace' => $this->namespace . '\Http\Controllers', 'middleware' => 'web'
        ], function ($router) {
            require  source_path('Http/routes.php');
        });
    }

    /**
     * Define the "web" routes for the package.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapPackageRoutes(array $params, $callback)
    {
        $domain = Projectfly::domain();
        $subdomain = $this->package->subdomain . '.' . $domain;

        $this->router->group(array_merge(['domain' => $subdomain], $params), $callback);
    }
}
