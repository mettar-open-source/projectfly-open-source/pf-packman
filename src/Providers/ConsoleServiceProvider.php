<?php

namespace Projectfly\Packman\Providers;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->registerMigrateCommand();
        $this->registerMigrateRefreshCommand();
        $this->registerMigrateResetCommand();
        $this->registerSeedCommand();
    }

    /**
     * Register the package:migrate command.
     */
    protected function registerMigrateCommand()
    {
        $this->app->singleton('command.package.migrate', function ($app) {
            return new \Projectfly\Packman\Console\Commands\PackageMigrateCommand($app['migrator'], $app['packages']);
        });

        $this->commands('command.package.migrate');
    }

    /**
     * Register the package:migrate:refresh command.
     */
    protected function registerMigrateRefreshCommand()
    {
        $this->app->singleton('command.package.migrate.refresh', function () {
            return new \Projectfly\Packman\Console\Commands\PackageMigrateRefreshCommand();
        });

        $this->commands('command.package.migrate.refresh');
    }

    /**
     * Register the package:migrate:reset command.
     */
    protected function registerMigrateResetCommand()
    {
        $this->app->singleton('command.package.migrate.reset', function ($app) {
            return new \Projectfly\Packman\Console\Commands\PackageMigrateResetCommand($app['packages'], $app['files'], $app['migrator']);
        });

        $this->commands('command.package.migrate.reset');
    }


    /**
     * Register the package:seed command.
     */
    protected function registerSeedCommand()
    {
        $this->app->singleton('command.package.seed', function ($app) {
            return new \Projectfly\Packman\Console\Commands\PackageSeedCommand($app['packages']);
        });
        $this->commands('command.package.seed');
    }
}
