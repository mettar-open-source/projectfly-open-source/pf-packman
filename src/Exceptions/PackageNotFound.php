<?php

namespace Projectfly\Packman\Exceptions;

class PackageNotFound extends \Exception
{
}
