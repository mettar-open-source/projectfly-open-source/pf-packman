<?php

namespace Projectfly\Packman;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Projectfly\Packman\Repository\Package;

class PackmanServiceProvider extends ServiceProvider
{
    /**
     * This contains all the enabled packages.
     *
     * @var array
     */
    private $packages;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // parent::boot();
    }

    /**
     * Register Packages.
     *
     * @return void
     */
    public function register()
    {
        $this->packages = collect();

        $this->sync_packages();
        foreach ($this->packages as $package) {
            $package->bootstrap();
        }

        $packages = $this->packages;

        $this->app->singleton('packages', function () use ($packages) {
            return new \Projectfly\Packman\Repository\Packages($packages);
        });

        $this->app->register('Projectfly\Packman\Providers\MigrationServiceProvider');
        $this->app->register('Projectfly\Packman\Providers\ConsoleServiceProvider');
    }

    /**
     * Sync Packages.
     *
     * @return void
     */
    public function sync_packages()
    {
        $includes = config('projectfly.packages.include');
        $dev_includes = config('projectfly.packages.include-dev');

        if (config('projectfly.packages.dev')) {
            foreach ($dev_includes as $include) {
                $this->include($include, true);
            }
        }
        foreach ($includes as $include) {
            $this->include($include);
        }
    }

    /**
     * Include Packages.
     *
     * @return void
     */
    private function include($include, $is_dev = false)
    {
        if (File::exists($include)) {
            $directories = File::directories($include);
            foreach ($directories as $directory) {
                $package = new Package($directory, $is_dev);
                if (!File::exists($package->base_path('packman.json'))) {
                    continue;
                }
                
                if ($this->packages->where('slug', $package->slug)->isEmpty()) {
                    $this->packages->push($package);
                }
            }
        }
    }
}
