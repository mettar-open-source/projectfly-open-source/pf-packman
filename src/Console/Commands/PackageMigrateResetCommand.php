<?php

namespace Projectfly\Packman\Console\Commands;

use Projectfly\Packman\Repository\Packages;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PackageMigrateResetCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:migrate:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback all database migrations for a specific or all packages';

    /**
     * @var Packages
     */
    protected $packages;

    /**
     * @var Migrator
     */
    protected $migrator;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * Create a new command instance.
     *
     * @param Packages    $packages
     * @param Filesystem $files
     * @param Migrator   $migrator
     */
    public function __construct(Packages $packages, Filesystem $files, Migrator $migrator)
    {
        parent::__construct();

        $this->packages = $packages;
        $this->files = $files;
        $this->migrator = $migrator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->confirmToProceed()) {
            return;
        }

        if (!empty($this->argument('slug'))) {
            $package = $this->packages->findBySlug($this->argument('slug'));
            return $this->reset($package);
        }

        foreach ($this->packages->all() as $package) {
            $this->reset($package);
        }
    }

    /**
     * Run the migration reset for the specified package.
     *
     * Migrations should be reset in the reverse order that they were
     * migrated up as. This ensures the database is properly reversed
     * without conflict.
     *
     * @param package $package
     * @return mixed
     */
    protected function reset($package)
    {
        $this->migrator->setconnection($this->input->getOption('database'));

        $pretend = $this->input->getOption('pretend');
        $migrationPath = $this->getMigrationPath($package);
        $migrations = array_reverse($this->migrator->getMigrationFiles($migrationPath));

        if (count($migrations) == 0) {
            return $this->error('Nothing to rollback.');
        }

        foreach ($migrations as $migration) {
            $this->info('Migration: '.$migration);
            $this->runDown($package, $migration, $pretend);
        }
    }

    /**
     * Run "down" a migration instance.
     *
     * @param package $package
     * @param object $migration
     * @param bool   $pretend
     */
    protected function runDown($package, $migration, $pretend)
    {
        $migrationPath = $this->getMigrationPath($package);
        $file = (string) $migrationPath.'/'.$migration.'.php';
        $classFile = implode('_', array_slice(explode('_', basename($file, '.php')), 4));
        $class = studly_case($classFile);
        $table = $this->laravel['config']['database.migrations'];

        include $file;

        $instance = new $class();
        $instance->down();

        $this->laravel['db']->table($table)
            ->where('migration', $migration)
            ->delete();
    }

    /**
     * Get the console command parameters.
     *
     * @param package $package
     * @return array
     */
    protected function getParameters($package)
    {
        $params = [];

        $params['--path'] = $this->getMigrationPath($package);

        if ($option = $this->option('database')) {
            $params['--database'] = $option;
        }

        if ($option = $this->option('pretend')) {
            $params['--pretend'] = $option;
        }

        if ($option = $this->option('seed')) {
            $params['--seed'] = $option;
        }

        return $params;
    }

    /**
     * Get migration directory path.
     *
     * @param package $package
     * @return string
     */
    protected function getMigrationPath($package)
    {
        return $package->source_path('Database/migrations/');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'Package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to use.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
            ['pretend', null, InputOption::VALUE_OPTIONAL, 'Dump the SQL queries that would be run.'],
            ['seed', null, InputOption::VALUE_OPTIONAL, 'Indicates if the seed task should be re-run.'],
        ];
    }
}
