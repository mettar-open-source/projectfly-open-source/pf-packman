<?php

namespace Projectfly\Packman\Console\Commands;

use App;
use Projectfly\Packman\Repository\Packages;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PackageMigrateCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the database migrations for a specific or all packages';

    /**
     * @var Packges
     */
    protected $packages;

    /**
     * @var Migrator
     */
    protected $migrator;

    /**
     * Create a new command instance.
     *
     * @param Migrator $migrator
     * @param Packages  $packages
     */
    public function __construct(Migrator $migrator, Packages $packages)
    {
        parent::__construct();

        $this->migrator = $migrator;
        $this->packages = $packages;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->prepareDatabase();

        if (!empty($this->argument('slug'))) {
            $package = $this->packages->findBySlug($this->argument('slug'));
            return $this->migrate($package);
        }

        foreach ($this->packages->all() as $package) {
            $this->migrate($package);
        }
    }

    /**
     * Run migrations for the specified package.
     *
     * @param package $package
     *
     * @return mixed
     */
    protected function migrate($package)
    {
        $pretend = Arr::get($this->option(), 'pretend', false);
        $path = $this->getMigrationPath($package);

        if (floatval(App::version()) > 5.1) {
            $pretend = ['pretend' => $pretend];
        }

        $this->migrator->run($path, $pretend);

        // Once the migrator has run we will grab the note output and send it out to
        // the console screen, since the migrator itself functions without having
        // any instances of the OutputInterface contract passed into the class.
        foreach ($this->migrator->getNotes() as $note) {
            if (!$this->option('quiet')) {
                $this->line($note);
            }
        }

        // Finally, if the "seed" option has been given, we will re-run the database
        // seed task to re-populate the database, which is convenient when adding
        // a migration and a seed at the same time, as it is only this command.
        if ($this->option('seed')) {
            $this->call('package:seed', ['slug' => $package->slug, '--force' => true]);
        }
    }

    /**
     * Get migration directory path.
     *
     * @param package $package
     * @return string
     */
    protected function getMigrationPath($package)
    {
        return $package->source_path('Database/migrations/');
    }

    /**
     * Prepare the migration database for running.
     */
    protected function prepareDatabase()
    {
        $this->migrator->setConnection($this->option('database'));

        if (!$this->migrator->repositoryExists()) {
            $options = array('--database' => $this->option('database'));

            $this->call('migrate:install', $options);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'Package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to use.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
            ['pretend', null, InputOption::VALUE_NONE, 'Dump the SQL queries that would be run.'],
            ['seed', null, InputOption::VALUE_NONE, 'Indicates if the seed task should be re-run.'],
        ];
    }
}
