<?php

namespace Projectfly\Packman\Console\Commands;

use Projectfly\Packman\Repository\Packages;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PackageSeedCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records for a specific or all packages';

    /**
     * @var Packages
     */
    protected $packages;

    /**
     * Create a new command instance.
     *
     * @param Packages $packages
     */
    public function __construct(Packages $packages)
    {
        parent::__construct();

        $this->packages = $packages;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!empty($this->argument('slug'))) {
            $package = $this->packages->findBySlug($this->argument('slug'));
            return $this->seed($package);
        }

        foreach ($this->packages->all() as $package) {
            $this->seed($package);
        }
    }

    /**
     * Seed the specific package.
     *
     * @param Package $package
     * @return array
     */
    protected function seed($package)
    {
        $params = [];
        $fullPath = $package->namespace('Database\Seeds\DatabaseSeeder');

        if (class_exists($fullPath)) {
            if ($this->option('class')) {
                $params['--class'] = $this->option('class');
            } else {
                $params['--class'] = $fullPath;
            }

            if ($option = $this->option('database')) {
                $params['--database'] = $option;
            }

            if ($option = $this->option('force')) {
                $params['--force'] = $option;
            }

            $this->call('db:seed', $params);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'Package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['class', null, InputOption::VALUE_OPTIONAL, 'The class name of the package\'s root seeder.'],
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to seed.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
        ];
    }
}
